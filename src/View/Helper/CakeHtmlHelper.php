<?php
namespace CakeHelper\View\Helper;
use Cake\Core\Configure;
use Cake\View\View;

/**
 * CakeHtml helper
 */
class CakeHtmlHelper extends \Bootstrap\View\Helper\HtmlHelper
{
    protected $cssLink = [
        'jquery-ui'       => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css',
        'bootstrap3'      => '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
        'bootstrap-theme' => '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css',
        'toastr'          => '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css',
        'animate'         => '//cdnjs.cloudflare.com/ajax/libs/animate.css/3.4.0/animate.min.css',
        'font-awesome'    => '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.0/css/font-awesome.min.css',
        'jasny'           => '//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css',
        'select2'         => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css',
        'selectize'       => '//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/css/selectize.min.css',
        'selectize-b3'    => '//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/css/selectize.bootstrap3.min.css',
        'bootstrap-datetimepicker' => '//cdnjs.cloudflare.com/ajax/libs/smalot-bootstrap-datetimepicker/2.3.11/css/bootstrap-datetimepicker.min.css'
    ];

    protected $jsLink = [
        'jquery'                    => '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
        'jquery-ui'                 => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
        'toastr'                    => '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js',
        'bootstrap3'                => '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        'accounting'                => '//cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js',
        'underscore'                => '//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min.js',
        'backbone'                  => '//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min.js',
        'moment'                    => '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment.min.js',
        'moment-fr'                 => '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/locale/fr.js',
        'twitch'                    => '//ttv-api.s3.amazonaws.com/twitch.min.js',
        'modernizr'                 => '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js',
        'jasny'                     => '//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js',
        'bootbox'                   => '//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js',
        'lazyload'                  => '//cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js',
        'select2'                   => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.full.min.js',
        'select2-fr'                => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/i18n/fr.js',
        'selectize'                 => '//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/js/standalone/selectize.min.js',
        'bootstrap-datetimepicker'  => '//cdn.rawgit.com/fafa973/bootstrap-datetimepicker/2.3.15/js/bootstrap-datetimepicker.min.js',
        'bootstrap-datetimepicker.fr'  => '//cdnjs.cloudflare.com/ajax/libs/smalot-bootstrap-datetimepicker/2.3.11/js/locales/bootstrap-datetimepicker.fr.js'
    ];

    public function addCss($key, $link)
    {
        $this->cssLink[$key] = $link;
    }

    public function addJs($key, $link)
    {
        $this->jsLink[$key] = $link;
    }

    public function css($path, array $options = [])
    {
        if (!is_array($path) && array_key_exists($path, $this->cssLink)) {
            return parent::css($this->cssLink[$path], $options);
        }
        return parent::css($path, $options);
    }

    public function script($url, array $options = [])
    {
        if (!is_array($url) && array_key_exists($url, $this->jsLink)) {
            return parent::script($this->jsLink[$url], $options);
        }
        return parent::script($url, $options);
    }

    public function link($title, $url = null, array $options = [])
    {
        if (isset($options['fa-icon'])) {
            $title = $this->tag('i', '', ['class' => "fa fa-{$options['fa-icon']}"]).' '.$title;
            $options['escape'] = false;
            unset($options['fa-icon']);
        }
        return parent::link($title, $url, $options);
    }

    public function boolean($value = null){
        if($value === null) return false;

        if($value) return '<i class="fa fa-check"></i>';
        return '<i class="fa fa-close"></i>';
    }

    public function cnil($cookieName, $url = null, $class = null)
    {
        if($url == null) $url = $this->Url->build(['controller' => 'pages', 'action' => 'cnil']);
        if($class == null) $class = 'alert-cnil';
        return $this->_View->element('CakeHelper.cnil', ['name' => $cookieName, 'class' => $class, 'url' => $url]);
    }

    public function ga($code = null)
    {
        if($code == null) $code = Configure::read('GoogleAnalytics.code');
        if(!$code) return trigger_error('GoogleAnalytics code is not set.');
        if(Configure::read('debug') > 0) return false;

        $out = "
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', '$code', 'auto');
        ga('send', 'pageview');
        ";
        return $this->scriptBlock($out);
    }

    public function addThis($code = null){
        if($code == null) $code = Configure::read('AddThis.code');
        if(!$code) return trigger_error('AddThis code is not set.');
        if(Configure::read('debug') > 0) return false;

        return $this->script("//s7.addthis.com/js/300/addthis_widget.js#pubid=$code", ['async' => true, 'defer' => true, 'inline' => false]);
    }
}
