<?php if (!isset($_COOKIE[$name])): ?>
    <?php
    $code = "
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = 'expires = '+d.toUTCString();
            document.cookie = cname + ' = ' + cvalue + '; ' + expires;
        }

        $('#alert-cnil')
        .on('close.bs.alert', function () {
            setCookie('$name', 1, 90);
        })
        .on('click', function () {
            $(this).alert('close');
        });
    ";
    $this->Html->scriptBlock($code, ['inline' => false]);
    echo $this->Html->tag('style', file_get_contents($this->Url->css('CakeHelper.cnil', ['fullBase' =>true]))); ?>
    <div id="alert-cnil" class="alert alert-dismissible navbar-fixed-bottom <?= $class; ?>"
         role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <strong><i class="fa fa-info-circle"></i></strong>
        En poursuivant votre navigation sur ce site,
        vous acceptez l’utilisation de cookie pour vous proposer, réaliser des statistiques de visites.
        <a href="<?= $url; ?>" class="alert-link">Cliquez ici,</a> pour en savoir plus.
    </div>
<?php endif; ?>