<?php
/**
 * @var \<%= $namespace %>\View\AppView $this
 * @var \<%= $namespace %>\Model\Entity\<%= $entityClass %>[]|\Cake\Collection\CollectionInterface $<%= $pluralVar %>
 */
?>
<%
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return !in_array($schema->columnType($field), ['binary', 'text']);
    });

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
    $fields = $fields->reject(function ($field) {
        return $field === 'lft' || $field === 'rght';
    });
}

if (!empty($indexColumns)) {
    $fields = $fields->take($indexColumns);
}

%>

<?php
$this->extend('CakeHelper./Common/view');
$this->assign('page-title', __('<%= $singularHumanName %>'));

$this->start('title-icon'); ?>
<i class='fa fa-list fa-lg' aria-hidden='true'></i>
<?php
$this->end();

$this->start('actions'); ?>
<div class='actions'>
    <ul class='nav nav-pills'>
        <li>
            <?= $this->Html->link('', ['action' => 'add'],['fa-icon' => 'plus-circle']); ?>
        </li>
    </ul>
</div>
<?php $this->end(); ?>

<div class="<%= $pluralVar %> index">
    <div class="panel table-responsive">
        <table class="table table-hover table-striped" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
<% foreach ($fields as $field): %>
                <th scope="col"><?= $this->Paginator->sort('<%= $field %>') ?></th>
<% endforeach; %>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody data-link="row" class="rowlink">
            <?php foreach ($<%= $pluralVar %> as $<%= $singularVar %>): ?>
            <tr>
<%          $pk = '$' . $singularVar . '->' . $primaryKey[0];
            foreach ($fields as $field) {
            $isKey = false;
            if (!empty($associations['BelongsTo'])) {
                foreach ($associations['BelongsTo'] as $alias => $details) {
                    if ($field === $details['foreignKey']) {
                        $isKey = true;
%>
                <td class='rowlink-skip'><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></td>
<%
                        break;
                    }
                }
            }
            if ($isKey !== true) {
                if($field == 'id') {
%>
                <td><?= $this->Html->link($<%= $singularVar %>-><%= $field %>, ['action' => 'view', <%= $pk %>]); ?></td>
<%
                } elseif($field == 'created' || $field == 'modified') {
%>
                <td><?= $this->Time->nice($<%= $singularVar %>-><%= $field %>) ?></td>
<%
                } elseif (!in_array($schema->columnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
%>
                <td><?= h($<%= $singularVar %>-><%= $field %>) ?></td>
<%
                } else {
%>
                <td><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></td>
<%
                }
            }
        }
%>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', <%= $pk %>], ['class' => 'btn btn-primary']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', <%= $pk %>], ['class' => 'btn btn-info']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', <%= $pk %>], ['class' => 'btn btn-danger', 'confirm' => __('Are you sure you want to delete # {0}?', <%= $pk %>)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>