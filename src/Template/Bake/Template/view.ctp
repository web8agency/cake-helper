<?php
/**
 * @var \<%= $namespace %>\View\AppView $this
 * @var \<%= $namespace %>\Model\Entity\<%= $entityClass %> $<%= $singularVar %>
 */
?>
<%
use Cake\Utility\Inflector;

$associations += ['BelongsTo' => [], 'HasOne' => [], 'HasMany' => [], 'BelongsToMany' => []];
$immediateAssociations = $associations['BelongsTo'];
$associationFields = collection($fields)
    ->map(function($field) use ($immediateAssociations) {
        foreach ($immediateAssociations as $alias => $details) {
            if ($field === $details['foreignKey']) {
                return [$field => $details];
            }
        }
    })
    ->filter()
    ->reduce(function($fields, $value) {
        return $fields + $value;
    }, []);

$groupedFields = collection($fields)
    ->filter(function($field) use ($schema) {
        return $schema->columnType($field) !== 'binary';
    })
    ->groupBy(function($field) use ($schema, $associationFields) {
        $type = $schema->columnType($field);
        if (isset($associationFields[$field])) {
            return 'string';
        }
        if (in_array($type, ['integer', 'float', 'decimal', 'biginteger'])) {
            return 'number';
        }
        if (in_array($type, ['date', 'time', 'datetime', 'timestamp'])) {
            return 'date';
        }
        return in_array($type, ['text', 'boolean']) ? $type : 'string';
    })
    ->toArray();

$groupedFields += ['number' => [], 'string' => [], 'boolean' => [], 'date' => [], 'text' => []];
$pk = "\$$singularVar->{$primaryKey[0]}";
%>

<?php
$this->extend('CakeHelper./Common/view');
$this->assign('page-title', __('<%= $singularHumanName %>'));

$this->start('title-icon'); ?>
<i class='fa fa-list fa-lg' aria-hidden='true'></i>
<?php
$this->end();

$this->start('actions'); ?>
<div class='actions'>
    <ul class='nav nav-pills'>
        <li>
            <?= $this->Html->link('', ['action' => 'edit', <%= $pk %>],['fa-icon' => 'pencil']); ?>
        </li>
    </ul>
</div>
<?php $this->end(); ?>

<div class="<%= $pluralVar %> view well">
	<dl class="dl-horizontal">
<% if ($groupedFields['number']) : %>
<% foreach ($groupedFields['number'] as $field) : %>
        <dt><?= __('<%= Inflector::humanize($field) %>') ?></dt>
        <dd><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></dd>
<% endforeach; %>
<% endif; %>
<% if ($groupedFields['string']) : %>
<% foreach ($groupedFields['string'] as $field) : %>
<% if (isset($associationFields[$field])) :
        $details = $associationFields[$field];
%>
        <dt><?= __('<%= Inflector::humanize($details['property']) %>') ?></dt>
        <dd><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></dd>
<% else : %>
        <dt><?= __('<%= Inflector::humanize($field) %>') ?></dt>
        <dd><?= h($<%= $singularVar %>-><%= $field %>) ?></dd>
<% endif; %>
<% endforeach; %>
<% endif; %>
<% if ($associations['HasOne']) : %>
    <%- foreach ($associations['HasOne'] as $alias => $details) : %>
        <dt><?= __('<%= Inflector::humanize(Inflector::singularize(Inflector::underscore($alias))) %>') ?></dt>
        <dd><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></dd>
    <%- endforeach; %>
<% endif; %>
<% if ($groupedFields['date']) : %>
<% foreach ($groupedFields['date'] as $field) : %>
        <dt><%= "<%= __('" . Inflector::humanize($field) . "') %>" %></dt>
        <dd><?= h($<%= $singularVar %>-><%= $field %>) ?></dd>
<% endforeach; %>
<% endif; %>
<% if ($groupedFields['boolean']) : %>
<% foreach ($groupedFields['boolean'] as $field) : %>
        <dt><?= __('<%= Inflector::humanize($field) %>') ?></dt>
        <dd><?= $<%= $singularVar %>-><%= $field %> ? __('Yes') : __('No'); ?></dd>
<% endforeach; %>
<% endif; %>
	</dl>
<% if ($groupedFields['text']) : %>
<% foreach ($groupedFields['text'] as $field) : %>
    <div class="row">
        <h4><?= __('<%= Inflector::humanize($field) %>') ?></h4>
        <?= $this->Text->autoParagraph(h($<%= $singularVar %>-><%= $field %>)); ?>
    </div>
<% endforeach; %>
<% endif; %>
</div>

<%
$relations = $associations['HasMany'] + $associations['BelongsToMany'];
foreach ($relations as $alias => $details):
    $otherSingularVar = Inflector::variable($alias);
    $otherPluralHumanName = Inflector::humanize(Inflector::underscore($details['controller']));
    %>
<div class="panel panel-primary related">
    <div class="panel-heading">
        <?= __('Related <%= $otherPluralHumanName %>') ?>
        <span class="pull-right">
            <?= $this->Html->link('', [
                'controller' => '<%= $details['controller'] %>',
                'action' => 'add'
            ],['class' => 'badge', 'fa-icon' => 'plus-circle fa-lg']); ?>
        </span>
    </div>
    <?php if (!empty($<%= $singularVar %>-><%= $details['property'] %>)): ?>
    <div class="table-responsive">
        <table class="table" cellpadding = "0" cellspacing = "0">
            <tr>
                <% foreach ($details['fields'] as $field): %>
                <th scope="col"><?= __('<%= Inflector::humanize($field) %>') ?></th>
                <% endforeach; %>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($<%= $singularVar %>-><%= $details['property'] %> as $<%= $otherSingularVar %>): ?>
            <tr>
                <%- foreach ($details['fields'] as $field): %>
                <td><?= h($<%= $otherSingularVar %>-><%= $field %>) ?></td>
                <%- endforeach; %>
                <%- $otherPk = "\${$otherSingularVar}->{$details['primaryKey'][0]}"; %>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => '<%= $details['controller'] %>', 'action' => 'view', <%= $otherPk %>], ['class' => 'btn btn-primary']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => '<%= $details['controller'] %>', 'action' => 'edit', <%= $otherPk %>], ['class' => 'btn btn-info']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => '<%= $details['controller'] %>', 'action' => 'delete', <%= $otherPk %>], ['class' => 'btn btn-danger', 'confirm' => __('Are you sure you want to delete # {0}?', <%= $otherPk %>)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <?php endif; ?>
</div>
<% endforeach; %>
