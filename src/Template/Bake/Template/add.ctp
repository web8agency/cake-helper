<?php
/**
  * @var \<%= $namespace %>\View\AppView $this
  */
    $this->assign('page-title', __('Add <%= $singularHumanName %>'));
?>
<%= $this->element('form');
