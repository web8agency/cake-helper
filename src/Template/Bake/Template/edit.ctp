<?php
/**
  * @var \<%= $namespace %>\View\AppView $this
  */
    $this->assign('page-title', __('Edit <%= $singularHumanName %>'));
?>
<%= $this->element('form');
