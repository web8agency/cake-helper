<%
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return $schema->columnType($field) !== 'binary';
    });

    if (isset($modelObject) && $modelObject->hasBehavior('Tree')) {
        $fields = $fields->reject(function ($field) {
            return $field === 'lft' || $field === 'rght';
        });
    }
%>

<?php
$this->extend('CakeHelper./Common/view');

$this->start('title-icon'); ?>
<i class='fa fa-list fa-lg' aria-hidden='true'></i>
<?php $this->end(); ?>

<div class="well <%= $pluralVar %> form">
    <?= $this->Form->create($<%= $singularVar %>) ?>
        <?php
<%
        foreach ($fields as $field) {
            if (in_array($field, $primaryKey)) {
                continue;
            }
            if (isset($keyFields[$field])) {
                $fieldData = $schema->column($field);
                if (!empty($fieldData['null'])) {
%>
                    echo $this->Form->control('<%= $field %>', ['options' => $<%= $keyFields[$field] %>, 'empty' => true]);
<%
                } else {
%>
                    echo $this->Form->control('<%= $field %>', ['options' => $<%= $keyFields[$field] %>]);
<%
                }
                continue;
            }
            if (!in_array($field, ['created', 'modified', 'updated'])) {
                $fieldData = $schema->column($field);
                if (in_array($fieldData['type'], ['date', 'datetime', 'time']) && (!empty($fieldData['null']))) {
%>
                    echo $this->Form->control('<%= $field %>', ['empty' => true]);
<%
                } else {
%>
                    echo $this->Form->control('<%= $field %>');
<%
                }
            }
        }
        if (!empty($associations['BelongsToMany'])) {
            foreach ($associations['BelongsToMany'] as $assocName => $assocData) {
%>
                echo $this->Form->control('<%= $assocData['property'] %>._ids', ['options' => $<%= $assocData['variable'] %>]);
<%
            }
        }
%>
        ?>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
