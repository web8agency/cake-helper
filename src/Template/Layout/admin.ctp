<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?= $this->fetch('title'); ?></title>

    <?= $this->Html->meta('icon'); ?>
    <?= $this->Html->meta(['name' => 'robots', 'content' => 'noindex']);; ?>
    <?= $this->fetch('meta') ?>

    <?= $this->Html->css('font-awesome'); ?>
    <?= $this->Html->css('jasny'); ?>
    <?= $this->Html->css('CakeHelper.styles'); ?>
    <?= $this->fetch('css'); ?>
</head>
<body>
<div class="navbar navbar-fixed-top navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/"><?= $this->Html->image('logo-mini.png'); ?></a>
            <button type="button" class="navbar-toggle navbar-offcanvas-toggle" data-toggle="offcanvas"
                    data-target="#sidebar" data-canvas="#content">
                <i class="fa fa-bars"></i>
            </button>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <i class="fa fa-ellipsis-h"></i>
            </button>
        </div>

        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <?= $this->fetch('navbar'); ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <span class="caret"></span> Name
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <?= $this->Html->link(__('Changer mon mot de passe'), [
                                'controller' => 'users',
                                'action' => 'account',
                                'admin' => false
                            ], ['fa-icon' => 'fa fa-lock']); ?>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <?= $this->Html->link(__('Se déconnecter'), [
                                'controller' => 'users',
                                'action' => 'logout',
                                'admin' => false
                            ], ['fa-icon' => 'fa fa-sign-out']); ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

<div id="sidebar" class="navmenu navmenu-default offcanvas-xs navmenu-fixed-left animated slideInLeft"
     role="navigation">
    <ul class="nav">
        <li>
            <?= $this->Html->link(__('TABLEAU DE BORD'), [
                'controller' => 'dashboard',
                'action' => 'index'
            ],
                [
                    'fa-icon' => "tachometer fa-lg fa-fw"
                ]); ?>
        </li>
        <li>
            <?= $this->Html->link(__('UTILISATEURS'), [
                'controller' => 'users',
                'action' => 'index'
            ],
                [
                    'fa-icon' => "user fa-lg fa-fw"
                ]); ?>
        </li>
        <?= $this->fetch('sidebar'); ?>
    </ul>
</div>

<div class="container-fluid" id="content">
    <?= $this->Flash->render('auth'); ?>
    <?= $this->Flash->render(); ?>

    <?= $this->fetch('content'); ?>
</div>

<?= $this->Html->script('jquery'); ?>
<?= $this->Html->script('bootstrap3'); ?>
<?= $this->Html->script('jasny'); ?>
<?= $this->fetch('script'); ?>
</body>
</html>