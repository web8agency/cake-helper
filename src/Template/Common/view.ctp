<?php $this->layout = 'CakeHelper.admin'; ?>
<div class="page-header">
    <h1><?= $this->fetch('title-icon'); ?> <?= $this->fetch('page-title'); ?>
        <?php if ($this->fetch('actions')): ?>
            <small class="pull-right"><?= $this->fetch('actions'); ?></small>
        <?php endif; ?>
    </h1>
</div>

<?php if ($this->fetch('help')): ?>
    <div class="well well-sm well-info">
        <div class="row">
            <div class="col-xs-2 col-sm-1 text-center">
                <i class="fa fa-question-circle fa-3x"></i>
            </div>
            <div class="col-xs-10 col-sm-11 text-justify">
                <p class="text-primary"><?= $this->fetch('help'); ?></p>
            </div>
        </div>
    </div>
<?php endif; ?>

<?= $this->fetch('content'); ?>

<?= $this->fetch('footer'); ?>