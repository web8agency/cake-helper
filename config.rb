require 'bootstrap-sass'
require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
sass_dir = 'sass'
css_dir = "webroot/css"
images_dir = "webroot/img"
javascripts_dir = "webroot/js"
fonts_dir = "webroot/fonts"
http_stylesheets_path = 'cake_helper/css'
http_javascripts_path = 'cake_helper/js'
http_images_path = 'cake_helper/img'
http_fonts_path = 'cake_helper/fonts'
environment = :development
output_style = :compact




# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass
