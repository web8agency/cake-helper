<?php
use Cake\Core\Configure;
use Cake\Core\Plugin;

Plugin::load('Bootstrap');
Configure::write('CakeHelperPlaceholderPath', '/cake_helper/img/placeholder.gif');

Configure::write('CakeHelperOnline', true);