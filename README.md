# CakeHelper v2.0.2

CakeHelper is a plugin for CakePHP 3.x using Bootstrap 

* [Bootstrap(3.0.0)](http://getbootstrap.com/)

## Requirements

* CakePHP >= 3.x
* Bootstrap >= 3.0

## Installation

Ensure require is present in composer.json. This will install the plugin into Plugin/CakeHelper:

	{
		"require": {
			"web8agency/cake-helper": "*"
		}
	}

### Enable plugin

You need to enable the plugin in your app/Config/bootstrap.php file:

`Plugin::load('CakeHelper', ['bootstrap' => true]);`


### Enable Offline 

If you want to use librairies on webroot folder, add to your bootstrap.php

	Configure::write('CakeHelperOnline', false);