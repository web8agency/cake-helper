## [2.0] - 2017-06-13
### Updated
- update all project for CakePhp 3.x

## [1.4] - 2017-03-22
### Updated
- load boostCake

## [1.3] - 2017-02-16
### Added
- cnil helper, ga helper, addthis helper and bake templates

## [1.2.5] - 2016-11-16
### Updated
- github link datetimepicker js firefox compatibility 

## [1.2.4] - 2016-11-16
### Updated
- github link datetimepicker js viewDate on changeView

## [1.2.2] - 2016-09-20
### Updated
- github link datetimepicker css and js with dates disabled

## [1.2.1] - 2016-09-20
### Updated
- github link datetimepicker with dates disabled

## [1.2] - 2016-09-19
### Added
- add bootstrap datetimepicker with dates disabled

## [1.1.2] - 2016-09-17
### Updated
- CakeHtmlHelper: update bootstrap and momentJS version.

## [1.1.1] - 2016-09-17
### Added
- CakeHtmlHelper: add boolean function.

## [1.1.0] - 2016-09-17
### Added
- CakeHelperTime class in Utility folder.